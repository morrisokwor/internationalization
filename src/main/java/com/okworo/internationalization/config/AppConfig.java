package com.okworo.internationalization.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

import java.util.Locale;


/**
 * @author Morris.Okworo on 16/06/2024
 */
@Configuration
public class AppConfig {

    @Bean
    public ResourceBundleMessageSource messageSource() {
        var source = new ResourceBundleMessageSource();
        source.setBasenames("messages/messages"); //where the messages are stored
        source.setDefaultEncoding("UTF-8"); //specifies the character encoding to use when reading the properties files. UTF-8 is a good choice as it can represent all characters from all languages.
        source.setCacheSeconds(3600); // ensures that if a specific message is not found for a given code, the code itself will be returned as the default message
        source.setUseCodeAsDefaultMessage(true); // sets the caching period for the messages to avoid reading from the properties files on every request, thereby improving performance
        return source;
    }


    @Bean
    public LocaleResolver localeResolver() {
        SessionLocaleResolver slr = new SessionLocaleResolver();
        slr.setDefaultLocale(Locale.ENGLISH);
        return slr;
    }
}
