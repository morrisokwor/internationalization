package com.okworo.internationalization.services;

import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import java.util.Locale;
import java.util.Optional;

/**
 * @author Morris.Okworo on 16/06/2024
 */
@Service
@RequiredArgsConstructor
public class TranslationService {

    private final MessageSource messageSource;


    public String greet(HttpServletRequest request, String firstName, String lastName) {
        Optional<String> langHeader = Optional.ofNullable(request.getHeader("Accept-Language"));
        if (!langHeader.isPresent()) {
            throw new RuntimeException(messageSource.getMessage(" specify.lang", null, Locale.getDefault()));
        }
        Locale locale = new Locale(langHeader.get());

        var greetings = messageSource.getMessage("welcome.greet", new String[]{firstName, lastName}, locale);
        return greetings;
    }
}
