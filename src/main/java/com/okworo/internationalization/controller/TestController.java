package com.okworo.internationalization.controller;

import com.okworo.internationalization.services.TranslationService;
import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Morris.Okworo on 16/06/2024
 */
@RestController
@RequestMapping("test")
@RequiredArgsConstructor
public class TestController {
    private final TranslationService translationService;

    @GetMapping("greet")
    public ResponseEntity<?> greet(HttpServletRequest request, @RequestParam String firstName, @RequestParam String lastName) {
        return ResponseEntity.ok(translationService.greet(request, firstName, lastName));
    }
}
